#
# Variables Configuration
#
variable "zones" {
  default = [
    "us-east-1b",
    "us-east-1c",
    "us-east-1d"
  ]
  type = "list"
}
variable "cluster-name" {
  default = "terraform-eks-eks"
  type    = "string"
}


variable "access_key" {
  default = "Enter your access_key"
  type    = "string"
}

variable "secret_key" {
  default = "Enter your secret_key"
  type    = "string"
}
